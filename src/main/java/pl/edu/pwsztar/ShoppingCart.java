package pl.edu.pwsztar;

import java.util.*;

public class ShoppingCart implements ShoppingCartOperation {
    Map<String, Product> cartProducts = new HashMap<String, Product>();

    public boolean addProducts(String productName, int price, int quantity) {
        if(productName == "" || price <= 0 || quantity < 0){
            return false;
        }

        if(quantity + getAllCartQuantity() > PRODUCTS_LIMIT){
            return false;
        }
        if(cartProducts.containsKey(productName)){
            if(cartProducts.get(productName).getPrice() == price){
                /* Add quantity to product that is already in cart */
                Product alreadyInCartProduct = cartProducts.get(productName);
                alreadyInCartProduct.addQuantity(quantity);
                cartProducts.replace(alreadyInCartProduct.getName(), alreadyInCartProduct);
                return true;
            }else{
                return false;
            }
        }
        cartProducts.put(productName, new Product(productName, price, quantity));
        return true;
    }

    public boolean deleteProducts(String productName, int quantity) {
        if(productName == "" || quantity < 0){
            return false;
        }

        Product cartProduct = cartProducts.get(productName);

        if(Optional.ofNullable(cartProduct).isPresent()){
            if(cartProduct.getQuantity() == quantity){
                /* Delete product */
                cartProducts.remove(productName);
                return true;
            }
            else if(cartProduct.getQuantity() > quantity){
                /* Decrease quantity */
                cartProduct.decreaseQuantity(quantity);
                return true;
            }
            else{
                return false;
            }
        }
        return false;
    }

    public int getQuantityOfProduct(String productName) {
        if(productName == ""){
            return 0;
        }

        Product product = cartProducts.get(productName);
        if(Optional.ofNullable(product).isPresent()){
            return product.getQuantity();
        }
        return 0;
    }

    public int getSumProductsPrices() {
        int sum = 0;

        sum += cartProducts.values().stream().mapToInt(product -> (product.getQuantity() * product.getPrice())).sum();

        return sum;
    }

    public int getProductPrice(String productName) {
        if(productName == ""){
            return 0;
        }

        Product product = cartProducts.get(productName);
        if(Optional.ofNullable(product).isPresent()){
            return product.getPrice();
        }

        return 0;
    }

    public int getAllCartQuantity(){
        int allCartProducts = cartProducts.values().stream().mapToInt(Product::getQuantity).sum();

        return allCartProducts;
    }

    public List<String> getProductsNames() {
        List<String> currentProducts = new ArrayList<>();

        cartProducts.values().forEach(product -> currentProducts.add(product.getName()));

        return currentProducts;
    }
}
