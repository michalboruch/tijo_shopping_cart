package pl.edu.pwsztar;

public class Main {

    public static void main( String[] argv ) {
        System.out.println("Shopping cart...");
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProducts("Tomato", 2, 2);
        shoppingCart.addProducts("Egg", 1, 12);
        shoppingCart.getProductsNames();
        shoppingCart.getAllCartQuantity();
        shoppingCart.getSumProductsPrices();
    }
}
