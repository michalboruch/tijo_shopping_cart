package pl.edu.pwsztar;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class AddProductTest {
    ShoppingCart shoppingCart = new ShoppingCart();

    @Test
    void test(){
        Product first = new Product("Tomato", 2, 2);
        Product second = new Product("Apple", 1, 491);
        Product third = new Product("Egg", 1, 509);
        Product fourth = new Product("Tomato", 2, 5);
        Product fifth = new Product("", 2, 5);

        assertEquals(true, shoppingCart.addProducts(first.getName(), first.getPrice(), first.getQuantity()));
        assertEquals(true, shoppingCart.addProducts(second.getName(), second.getPrice(), second.getQuantity()));
        assertEquals(false, shoppingCart.addProducts(third.getName(), third.getPrice(), third.getQuantity()));
        assertEquals(true, shoppingCart.addProducts(fourth.getName(), fourth.getPrice(), fourth.getQuantity()));
        assertEquals(false, shoppingCart.addProducts(fifth.getName(), fifth.getPrice(), fifth.getQuantity()));
    }
}
