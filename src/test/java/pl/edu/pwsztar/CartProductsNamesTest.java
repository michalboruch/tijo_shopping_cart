package pl.edu.pwsztar;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CartProductsNamesTest {

    @Test
    void test(){
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProducts("Apple",2,9);
        shoppingCart.addProducts("Egg",1,3);
        shoppingCart.addProducts("Milk",3,2);
        shoppingCart.addProducts("",11,1);
        shoppingCart.addProducts("Water",10,-1);

        List<String> items = new ArrayList<>();
        items.add("Apple");
        items.add("Egg");
        items.add("Milk");

        assertEquals(items, shoppingCart.getProductsNames());
    }
}
