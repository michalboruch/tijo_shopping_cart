package pl.edu.pwsztar;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ProductPriceTest {
    @Test
    void test(){
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProducts("Apple",2,9);
        shoppingCart.addProducts("Egg",1,3);
        shoppingCart.addProducts("Milk",3,2);
        shoppingCart.addProducts("",3,2);

        assertEquals(2, shoppingCart.getProductPrice("Apple"));
        assertEquals(1, shoppingCart.getProductPrice("Egg"));
        assertEquals(3, shoppingCart.getProductPrice("Milk"));
        assertEquals(0, shoppingCart.getProductPrice(""));
    }
}
