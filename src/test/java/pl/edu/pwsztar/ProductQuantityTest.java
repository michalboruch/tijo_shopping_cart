package pl.edu.pwsztar;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ProductQuantityTest {

    @Test
    void test(){
        ShoppingCart shoppingCart = new ShoppingCart();

        Product first = new Product("Egg", 1, 5);
        Product second = new Product("Milk", 3, 55);
        Product third = new Product("Apple", 2, 15);
        Product fourth = new Product("", 2, 15);

        shoppingCart.addProducts(first.getName(), first.getPrice(), first.getQuantity());
        shoppingCart.addProducts(second.getName(), second.getPrice(), second.getQuantity());
        shoppingCart.addProducts(third.getName(), third.getPrice(), third.getQuantity());

        assertEquals(5, shoppingCart.getQuantityOfProduct(first.getName()));
        assertEquals(55, shoppingCart.getQuantityOfProduct(second.getName()));
        assertEquals(15, shoppingCart.getQuantityOfProduct(third.getName()));

        shoppingCart.deleteProducts(first.getName(), 3);
        assertEquals(2, shoppingCart.getQuantityOfProduct(first.getName()));

        shoppingCart.addProducts(fourth.getName(), fourth.getPrice(), fourth.getQuantity());
        assertEquals(0, shoppingCart.getQuantityOfProduct(fourth.getName()));
    }


}
