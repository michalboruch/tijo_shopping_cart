package pl.edu.pwsztar;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class SumProductPriceTest {
    @Test
    void test(){
        ShoppingCart shoppingCart = new ShoppingCart();

        shoppingCart.addProducts("Apple",2,9);
        shoppingCart.addProducts("Egg",1,3);
        shoppingCart.addProducts("Milk",3,2);
        shoppingCart.addProducts("",3,2);

        assertEquals(27, shoppingCart.getSumProductsPrices());

        shoppingCart.deleteProducts("Milk", 1);

        assertEquals(24, shoppingCart.getSumProductsPrices());

        shoppingCart.deleteProducts("", 1);

        assertEquals(24, shoppingCart.getSumProductsPrices());
    }
}
