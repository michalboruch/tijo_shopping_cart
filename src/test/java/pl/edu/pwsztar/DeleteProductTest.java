package pl.edu.pwsztar;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class DeleteProductTest {

    @Test
    void test(){
        ShoppingCart shoppingCart = new ShoppingCart();

        Product first = new Product("Egg", 1, 5);
        Product second = new Product("Milk", 3, 55);
        Product third = new Product("Apple", 2, 15);
        Product fourth = new Product("", 2, 15);
        shoppingCart.addProducts(first.getName(), first.getPrice(), first.getQuantity());
        shoppingCart.addProducts(second.getName(), second.getPrice(), second.getQuantity());
        shoppingCart.addProducts(third.getName(), third.getPrice(), third.getQuantity());

        assertEquals(false, shoppingCart.deleteProducts("Egg", 7));
        assertEquals(false, shoppingCart.deleteProducts(third.getName(), 20));
        assertEquals(true, shoppingCart.deleteProducts("Apple", 10));
        assertEquals(true, shoppingCart.deleteProducts(second.getName(), 40));
        assertEquals(false, shoppingCart.deleteProducts(fourth.getName(), 40));
    }
}
